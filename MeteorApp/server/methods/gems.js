Meteor.methods({
    insertGem: function (latitude, longitude, description) {
        Gems.insert({
            createdAt: new Date(),
            latitude: latitude,
            longitude: longitude,
            description: description,
            likes: 0,
            dislikes: 0
        });
    }
});
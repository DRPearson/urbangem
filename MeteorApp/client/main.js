import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import './main.html';

Template.data.onCreated(function dataOnCreated() {
  Meteor.subscribe('gems');
});

Template.data.events({
  'submit'(event) {
    event.preventDefault();
    const target = event.target;
    const latitude = target.latitude.value;
    const longitude = target.longitude.value;
    const description = target.description.value;

    Meteor.call('insertGem', latitude, longitude, description, function (error, result) {
      console.log("Gem inserted into database " + result);
    });
  }
});

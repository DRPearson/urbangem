import React, { Component } from 'react';
import Meteor from 'react-native-meteor';
import MapView from 'react-native-maps';

import styles from './styles';
import {
    Text,
    View,
    TextInput,
} from 'react-native';

export default class Gem extends Component {
    constructor(props) {
        super(props);
    }

    convertToLocation(location) {
        return parseFloat(location);
    }

    like() {
        console.log("Like");
        Meteor.call('updateLikeCount', 1, )
    }

    dislike() {
        console.log("Dislike");
    }


    render() {
        return (
            <MapView.Marker coordinate={{
                latitude: this.convertToLocation(this.props.gem.latitude),
                longitude: this.convertToLocation(this.props.gem.longitude)
            }}>
                <MapView.Callout style={styles.plainView}>
                    <View>
                        <Text>{this.props.gem.description}</Text>
                    </View>
                </MapView.Callout>
            </MapView.Marker>
        )
    }
}


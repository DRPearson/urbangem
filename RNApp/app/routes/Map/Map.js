import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import MapView from 'react-native-maps';
import Gem from '../../components/Gem';
import Modal from 'react-native-modalbox';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../../config/styles';
import {
  Text,
  View,
  TextInput,
  StyleSheet
} from 'react-native';
import GenericTextInput from '../../components/GenericTextInput';
import Button from '../../components/Button';

const timeout = 4000;

class Map extends Component {
  constructor() {
    super();
    this.state = {
      initialPosition: 'unknown',
      lastPosition: 'unknown',
      description: "",
      isDisabled: false,
      subscription: {
        allGems: Meteor.subscribe("allgems")
      }
    }
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        var initialPosition = position;
        this.setState({ initialPosition });
      },
      (error) => alert(JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    this.watchID = navigator.geolocation.watchPosition((position) => {
      var lastPosition = position;
      this.setState({ lastPosition });
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  onSubmitGemPress() {
    Meteor.call('insertGem', this.state.lastPosition.coords.latitude, this.state.lastPosition.coords.longitude, this.state.description, function (error, result) {
      if (error) {
        console.log("Error inserting gem. Map.js onSubmitGemPress(). \n" + error);
      }
    });
    this.toggleDisable();
  }

  openModalGem() {
    this.refs.modalGem.open();
  }

  toggleDisable() {
    this.setState({ isDisabled: !this.state.isDisabled });
  }

  render() {
    const { currentGems } = this.props;
    if (this.state.initialPosition.coords === 'unknown' || this.state.initialPosition.coords === undefined) {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>Loading</Text>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <MapView.Animated ref={ref => { this.map = ref; } } style={styles.map} initialRegion={{
          latitude: this.state.initialPosition.coords.latitude,
          longitude: this.state.initialPosition.coords.longitude,
          latitudeDelta: 0.0060,
          longitudeDelta: 0.0060,
        }} showsUserLocation={true}>
          {currentGems.map((currentGem) => {
            return <Gem key={currentGem._id} gem={currentGem} />
          })}
        </MapView.Animated>
        <Button onPress={this.openModalGem.bind(this)} style={styles.btn} text="+Gem"></Button>
        <Modal style={[styles.modal, styles.modalGem]} position="center" ref="modalGem" isDisabled={this.state.isDisabled}>
          <Text style={styles.text}>Modal centered</Text>
          <TextInput style={styles.input} ref="description" borderTop={true} placeholder="Description" onChangeText={(text) => this.setState({ description: text })} />
          <Button text="Submit Gem" onPress={this.onSubmitGemPress.bind(this)} />
        </Modal>
      </View>
    )
  }

}

export default createContainer(params => {
  Meteor.subscribe('gems');
  return {
    currentGems: Meteor.collection('gems').find(),
  };
}, Map)

var styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 10
  },
  input: {
    height: 40,
    borderRadius: 5,
    backgroundColor: colors.inputBackground,
    marginLeft: 10,
    marginVertical: 5,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  divider: {
    height: 1,
    backgroundColor: colors.inputDivider,
    flex: 1,
    marginLeft: 10,
  },
  inputWrapper: {
    backgroundColor: colors.inputBackground,
    width: window.width,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalGem: {
    height: 300,
    width: 300
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  padding: 10
  }
});

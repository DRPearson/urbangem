Urban Gem is a social platform that displays real time events happening around you ('the gems hidden throughout the city').

It's built with React Native, Node, MongoDB, and uses Meteor on the backend to provide the pub/sub architecture.

V0.5 Currently is in development and allows manual creation of Gem objects by providing a lat, long, and title.
If you spawn multiple instances you will be able to see the map update in realtime. 